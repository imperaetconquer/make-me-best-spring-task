package aero.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aero.exception.AccountAlreadyExistsException;
import aero.exception.AccountNotFoundException;
import aero.repository.CustomerAccountRepository;
import aero.rest.CustomerAccountDTO;

@Service
public class CustomerAccountService {

	private final CustomerAccountRepository customerAccountRepository;

	private final ConverterService converterService;

	@Autowired
	public CustomerAccountService(CustomerAccountRepository customerAccountRepository,
			ConverterService converterService) {
		this.customerAccountRepository = customerAccountRepository;
		this.converterService = converterService;
	}

	public List<CustomerAccountDTO> getCustomerAccounts() {
		return customerAccountRepository.findAll().values().stream().map(converterService::fromCustomerAccountToDTO)
				.collect(Collectors.toList());
	}

	public CustomerAccountDTO createCustomerAccount(CustomerAccountDTO customerAccountDTO) throws AccountAlreadyExistsException {
		var customerAccount = customerAccountRepository.findByLogin(customerAccountDTO.getLogin());
		if (customerAccount.isPresent()) {
			throw new AccountAlreadyExistsException();
		} else {
			customerAccountRepository.save(converterService.fromDTOtoCustomerAccount(customerAccountDTO));
		}
		return customerAccountDTO;
	}

	public CustomerAccountDTO getCustomerAccount(String login) throws AccountNotFoundException {
		var customerAccount = customerAccountRepository.findByLogin(login).orElseThrow(AccountNotFoundException::new);
		return converterService.fromCustomerAccountToDTO(customerAccount);
	}

	public void updateCustomerAccount(CustomerAccountDTO newCustomerAccountDTO, String login)
			throws AccountNotFoundException {
		customerAccountRepository.findByLogin(login).orElseThrow(AccountNotFoundException::new);
		customerAccountRepository.save(converterService.fromDTOtoCustomerAccount(newCustomerAccountDTO));
	}

	public void deleteCustomerAccount(String login) throws AccountNotFoundException {
		customerAccountRepository.findByLogin(login).orElseThrow(AccountNotFoundException::new);
		customerAccountRepository.delete(login);
	}
}
