package aero.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import org.springframework.stereotype.Service;

import aero.domain.CustomerAccount;
import aero.rest.CustomerAccountDTO;

@Service
public class ConverterService {

	public CustomerAccount fromDTOtoCustomerAccount(CustomerAccountDTO customerAccountDTO) {
		var customerAccount = new CustomerAccount();
		customerAccount.setLogin(customerAccountDTO.getLogin());
		customerAccount.setPassword(customerAccountDTO.getPassword());
		customerAccount.setDateOfBirth(stringtoDate(customerAccountDTO.getDateOfBirth()));
		return customerAccount;
	}

	public CustomerAccountDTO fromCustomerAccountToDTO(CustomerAccount customerAccount) {
		var customerAccountDTO = new CustomerAccountDTO();
		customerAccountDTO.setLogin(customerAccount.getLogin());
		customerAccountDTO.setPassword(customerAccount.getPassword());
		customerAccountDTO.setDateOfBirth(dateToString(customerAccount.getDateOfBirth()));
		return customerAccountDTO;
	}

	private String dateToString(Date date) {
		return new SimpleDateFormat("dd-MM-yyyy").format(date);
	}

	private Date stringtoDate(String dateAsString) {
		try {
			return new SimpleDateFormat("dd-MM-yyyy").parse(dateAsString);
		} catch (ParseException e) {
			e.printStackTrace();
			return Date.from(Instant.MIN);
		}
	}
}
