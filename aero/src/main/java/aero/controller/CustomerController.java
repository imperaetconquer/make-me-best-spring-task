package aero.controller;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import aero.exception.AccountAlreadyExistsException;
import aero.exception.AccountNotFoundException;
import aero.rest.CustomerAccountDTO;
import aero.service.CustomerAccountService;

@RestController
public class CustomerController {

	private final CustomerAccountService customerAccountService;

	@Autowired
	public CustomerController(CustomerAccountService customerAccountService) {
		this.customerAccountService = customerAccountService;
	}

	@GetMapping("/customeraccounts")
	public List<CustomerAccountDTO> getCustomerAccounts() {
		return customerAccountService.getCustomerAccounts();
	}

	@GetMapping("/customeraccounts/{login}")
	public CustomerAccountDTO getCustomerAccount(@PathVariable String login) throws AccountNotFoundException {
		return customerAccountService.getCustomerAccount(login);
	}

	@PostMapping("/customeraccounts")
	public ResponseEntity<String> createCustomerAccount(@Valid @RequestBody CustomerAccountDTO customerAccountDTO)
			throws AccountAlreadyExistsException {
		customerAccountService.createCustomerAccount(customerAccountDTO);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PutMapping("/customeraccounts/{login}")
	public ResponseEntity<String> updateCustomerAccount(@Valid @RequestBody CustomerAccountDTO newCustomerAccountDTO,
			@PathVariable String login) throws AccountNotFoundException {
		customerAccountService.updateCustomerAccount(newCustomerAccountDTO, login);
		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/customeraccounts/{login}")
	public ResponseEntity<String> deleteCustomerAccount(@PathVariable String login) throws AccountNotFoundException {
		customerAccountService.deleteCustomerAccount(login);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}
}
