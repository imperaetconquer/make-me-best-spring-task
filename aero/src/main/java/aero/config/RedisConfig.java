package aero.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
public class RedisConfig {

	@Value("${redis.cloud.host}")
	private String host;

	@Value("${redis.cloud.port}")
	private String port;

	@Value("${redis.cloud.pass}")
	private String pass;

	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		var jedisConnectionFactory = new JedisConnectionFactory();
		jedisConnectionFactory.setHostName(host);
		jedisConnectionFactory.setPort(Integer.valueOf(port));
		jedisConnectionFactory.setPassword(pass);
		return jedisConnectionFactory;
	}

	@Bean
	public RedisTemplate<?, ?> redisTemplate() {
		var template = new RedisTemplate<byte[], byte[]>();
		template.setConnectionFactory(jedisConnectionFactory());
		return template;
	}
}