package aero.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import aero.domain.CustomerAccount;

import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;

@Repository
public class CustomerAccountRedisRepository implements CustomerAccountRepository {

	private static final String KEY = "customer_account";
	private RedisTemplate<String, CustomerAccount> redisTemplate;
	private HashOperations<String, String, CustomerAccount> hashOperations;

	@Autowired
	public CustomerAccountRedisRepository(RedisTemplate<String, CustomerAccount> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	@PostConstruct
	private void init() {
		hashOperations = redisTemplate.opsForHash();
	}

	@Override
	public Map<String, CustomerAccount> findAll() {
		return hashOperations.entries(KEY);
	}

	@Override
	public void save(CustomerAccount customerAccount) {
		hashOperations.put(KEY, customerAccount.getLogin(), customerAccount);
	}

	@Override
	public void delete(String login) {
		hashOperations.delete(KEY, login);
	}

	@Override
	public Optional<CustomerAccount> findByLogin(String login) {
		return Optional.ofNullable(hashOperations.get(KEY, login));
	}

}
