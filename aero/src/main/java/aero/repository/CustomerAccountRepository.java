package aero.repository;

import java.util.Map;
import java.util.Optional;

import aero.domain.CustomerAccount;

public interface CustomerAccountRepository {

	Map<String, CustomerAccount> findAll();

	void save(CustomerAccount customerAccount);

	void delete(String login);

	Optional<CustomerAccount> findByLogin(String login);

}
