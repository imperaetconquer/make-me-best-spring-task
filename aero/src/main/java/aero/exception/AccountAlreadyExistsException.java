package aero.exception;

public class AccountAlreadyExistsException extends Exception {

	private static final long serialVersionUID = 367935664238378475L;

	public AccountAlreadyExistsException() {
		super();
	}

	public AccountAlreadyExistsException(String message) {
		super(message);
	}

}
