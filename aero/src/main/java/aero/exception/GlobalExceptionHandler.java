package aero.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(AccountNotFoundException.class)
	public ResponseEntity<?> NotFoundException(AccountNotFoundException x) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), x.toString());
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(AccountAlreadyExistsException.class)
	public ResponseEntity<?> AlreadyExistsException(AccountAlreadyExistsException x) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), x.toString());
		return new ResponseEntity<>(errorDetails, HttpStatus.FORBIDDEN);
	}

}
