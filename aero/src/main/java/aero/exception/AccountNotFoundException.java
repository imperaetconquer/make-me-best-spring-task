package aero.exception;

public class AccountNotFoundException extends Exception {

	private static final long serialVersionUID = 367935664238378475L;

	public AccountNotFoundException() {
		super();
	}

	public AccountNotFoundException(String message) {
		super(message);
	}

}
